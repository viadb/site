# viadbtheme

[![pipeline status](https://gitlab.com/smokmedia/assets/viadb/badges/master/pipeline.svg)](https://gitlab.com/smokmedia/assets/viadb/-/commits/master)

Theme para las páginas VIADB y VIADBPOSTGRES, basado en el theme de [educenter-hugo](https://github.com/themefisher/educenter-hugo/). Algunas mejoras agregadas:

- Selección de colores desde la configuración
- Logo dinámico en el scroll
- Imágenes en el carousel
- Español integrado
- Secciones genéricas y no específicas a lo que es el tópico del theme original
- Posibilidad de añadir videos embedded en Sucess Stories

## Como buildear el sitio

```
$ git clone git@gitlab.com:viadb/site.git
$ cd site
$ make dev
```

Luego, desde un browser: `localhost:1313`.

Para acceder remoto, utilizar:

```
make dev-remote
```


[Original Documentation](https://documentation.themefisher.com/docs/educenter-hugo/).

## Organización del repositorio

Por el momento, theme y site están en el mismo repositorio. Todo lo que está en `site`, es el sitio y lo que está fuera, es el theme.
Cuando el theme esté estable y el sitio listo para ser publicado, la carpeta `site` estará en su repositorio propio y el theme quedará
limpio. Se puede publicar el theme, pero hay que crear un sitio default para el `exampleSite`.

Lo primero que hay que tocar es todo el contenido, tanto texto como imágenes (algunas ya fueron cambiadas). El contenido está principalmente en 
`site/content`, `site/data` y `site/i18n`.

También hay que rellenar bien el `config.toml`, que es el archivo de configuración principal.


## Main features

* **SEO Friendly** All codes are seo friendly. There is fields to put meta data and other seo parameters in every pages.
* **Multi Language** We have multi language support in premium version . Right now there is two language include in the theme , those are French and English. If you need more we can help you out.
* **Google Analytics** You can add you google analytics code in theme config file to connect with your google anlytics account .


## Licensing

This Theme is released under [Creative Commons Attribution 3.0 (CC-BY-3.0) License](https://creativecommons.org/licenses/by/3.0/)
If you want to remove the credit simply make a [donation](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GSG5G2YL3E5V4), so that we can run our contribution to hugo community.
