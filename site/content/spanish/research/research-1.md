---
title: "¡Síganos para estar al tanto de nuestros White Papers!"
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : ""
# Research image
image: "images/research/bluecircuit.jpg"
# type
type: "research"
# draft: true
---

### Acerca de nuestras investigaciones

Nuestros consultores han liderado numerosas investigaciones en varias compañias de renombre y continuaremos ese camino para publicar contenido original, desde Infraestructura hasta desarrollo interno de Base de Datos.

