---
title: "Sebastian Amin Fariña"
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "Developer"
# staff portrait
image: "images/staff/amin-1.jpeg"
# course
# course: "Database Architect"
# biography
bio: "Developer en ViaDB "
# interest
#interest: ["Computer Networking","Database Infraestructure","Database Architect"]
# contact info
contact:
  # contact item loop
  - name : "amin@viadb.ar"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "mailto:amin@viadb.ar"

  # contact item loop
  # - name : "+12 034 5876"
  #   icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
  #   link : "tel:+120345876"

  # contact item loop
  # - name : "Clark Malik"
  #   icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
  #   link : "#"

  # contact item loop
  - name : "Website"
    icon : "ti-world" # icon pack : https://themify.me/themify-icons
    link : "http://aminfarina.com.ar"

  - name : "@aminsebastianok"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/aminsebastianok"

  - name : "GitHub"
    icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "https://github.com/amin-farina"
  
  - name : Linkedin 
    icon : "ti-linkedin"
    link : "https://www.linkedin.com/in/sebasti%C3%A1n-amin-fari%C3%B1a-aab0ba1b3"

type: "staff"
---

### About Me

