---
title: "Emanuel Calvo"
# page title background image
bg_image: "images/staff/emanuel.jpg"
# meta description
description : "Fundador de ViaDB y actualmente trabajando en el campo de Infraestructuras de Alta Disponibilidad en OnGres."
email: "emanuel@viadb.ar"
# portrait
image: ""
social:
  - icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/3manuek"
  - icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "https://github.com/3manuek"
  - icon : "ti-linkedin"
    link : "https://www.linkedin.com/in/ecbcbcb/"
---

Fundador y CEO de ViaDB y actualmente trabajando en el campo de Infraestructuras de Alta Disponibilidad en OnGres.

Es también Líder del equipo de IT de la Fundación Apolo.