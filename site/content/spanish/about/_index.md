---
title: "Sobre Nosotros"
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# about image
image: "images/logos-new/new-logoviadb-1.png"
# meta description
description : ""

---

## Sobre Nosotros

ViaDB es una firma de consultores que apunta a proveer servicios de consultoría, gestión y desarrollo de Infraestructuras Programáticas para motores de Base de Datos de Código Libre. 

Tenemos presencia en Argentina (Buenos Aires) y España (Madrid).

Nos proponemos a traer servicios y tecnologías basados en el conocimiento de nivel internacional a las empresas nacionales. 

> Somos partners de __OnGres Inc.__ en Argentina, empresa de creciente reputación global.

Establecidos en el mercado por más de 10 años, sumando miembros en el equipo y comenzando una nueva etapa en la empresa, estableciendo nuevos asociados estratégicos. Todos nuestros profesionales han trabajado en Compañías de renombre internacional, manipulando y desarrollando infraestructuras de alta complejidad.

__¡Estamos Orgullosos de lo que hemos logrado hasta ahora y esperamos verlo en nuestra cartera de clientes!__
