---
title: "Cuales son los beneficios de usar Postgres como backend de Terraform?"
date: 2019-12-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "Requiere Postgres 9.5+ y Terraform 0.12"
# post thumbnail
image: "images/courses/Terraform-logo.png"
# post author
author: "Emanuel Calvo"
# taxonomy
categories: ["Base de Datos"]
tags: ["PostgreSQL", "Backend"]
# type
type: "post"
---

### Algunos antecedentes y conceptos

 Terraform Backends es una abstraccion que almacena un *estado* de archivo o tabla (como veremos más adelante) de la operación __apply__, manteniendo todos los recursos creados en un lugar consistente. La documentación de Backend menciona explícitamente esto:

    Working in a team: Backends can store their state remotely
    and protect that state with locks to prevent corruption.

Eso es cierto, aunque debemos considerar lo siguiente que se indica en la documentación del Modelo de coherencia de datos de Amazon S3, que cité:

    Amazon S3 provides read-after-write consistency for PUTS of new objets in
    your S3 bucket in all regions with one caveat. The caveat is that if you make a HEAD or GET
    request to the key name (to find if the object exists) before creating the object,
    Amazon S3 provides eventual consistency for read-after-write

    Amazon S3 offers eventual consistency for overwrite PUTS and DELETES in all regions.

    *Updates to a single key are atomic.* For example, if you PUT to an existing key, a subsequent
    read might return the old data or the update data, but it will never return corrupted or partial data.

La actualización de la clave atómica significa que las PUT son coherentes, pero hay otras consideraciones. De forma predeterminada, el backend de S3 no es coherente a *menos* que configure el backend de S3 a través de la tabla DynamoDB:

    Stores the state as a given key in a given bucket on Amazon S3.
    This backend also supports state locking and consistency checking via Dynamo DB,
    which can be enable by setting the dynamodb_table field to an existing DynamoDB table name.

Eso termina siendo dos servicios para hacer lo mismo: servir como un backend consistente. E incluso ha implementado una infraestructura dedicada a serializar el trabajo del equipo, probablemente desee considerar las siguientes ventajas de usarlo como su backend de Terraform:

* Sistema de bloqueo completo y rico, que lo hace consistente y una fuente de verdad muy estable.
* Streaming Replication para la duplicación de su Backend.
* Un solo backend (una instancia de base de datos), puede obtener tantos TF backends como desee (cada uno en su propia base de datos).

    Mantener sus base de datos backend reflejadas y aisladas es una buena práctica para eviar colisiones en el momento del aprovisionamiento y agrega prácticas de seguridad para evitar que varios proyectos colisionen o afecten a otros recursos.

### PREREQUISITOS

Recuerde, los backends son recursos preexistentes o recursos pre-Terraform, por lo que no puede automatizar su creación dentro del mismo backend (el problema del huevo y la gallina)

La más fácil es generar un DaaS (como Cloud SQL o RDS) con un tamaño mínimo y las copias de seguridad habilitadas. Esto devolverá un punto final, que se puede combinar para formar el FQDN para el servicio.

Por ejemplo, AWS proporciona el servicio RDS, que se puede configurar con un solo comando como el siguiente:

    awd rds create-db-instance --db-name --db-instance-class master-username --master-password

### Backend

La definición de backend podría ubicarse en su "__backend.tf__" archivo:

    terraform {
        backend "pg" {}
        required_version = "> 0.12.0"
    }

La cadena de conexión del backend podría definirse en tfvars. En el siguiente ejemplo, tomamos la variable de entorno que contiene la asignación conn_str de la cadena de la conexión y la usamos como parámetro para la "__terraform init__" fase:

    export PROJECT=<project>
    export BACKEND_CONNSTR="conn_str=postgres://user:pass@<URI>/<backend_project_db>"

En Makefile o script, puede conectarlo como este ejemplo:

    echo "$${BACKEND_CONNSTR}" > terraform/environments/$(ENV)/$(ENV).tfvars && \
    cd terraform/environments/$(ENV) && \
    terraform init \
        -backend-config=$(ENV).tfvars ...

