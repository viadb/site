---
title: "Sobrecarga estimada en Postgres_fdw"
date: 2017-03-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
# description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. dolore magna aliqua. Ut enim ad minim veniam, quis nostrud."
# post thumbnail
image: "images/blog/postgres.jpg"
# post author
author: "Emanuel Calvo"
# taxonomy
categories: ["Base de Datos"]
tags: ["Educacion", "PostgreSQL"]
# type
type: "post"
---

### Análisis de rendimiento de la sobrecarga de FDW en cargas de trabajo de transacciones intensivas

Los puntos de referencia actuales se ejecutaron en PostgreSQL 9.6.2, con el fin de obtener una estimación de la sobrecarga de la extensión "__postgres_fdw__". Para hacerlo, establecería cuatro bases de datos en dos esquemas diferentes en los que __source__ es la única base de datos que almacena los datos de pgbench. El resto de las bases de datos solo contienen el DDL de las tablas externas. Ambas instancias residen en la misma máquina para descargar cualquier sesgo de red en los resultados finales. Uno de los FDW se conecta desde la misma instancia (FDW_local) y los otros desde una instancia diferente (FDW_external y FDW_ext_ro), descripción en el anexo de referencia.

Tenga en cuenta que también que el "__pgbench__" implica una carga de trabajo de transacciones intensivas, que generalmente no es adecuada para yablas externas. Entonces, la sobrecarga es algo esperado en este punto.

De las pruebas realizadas, "__postgres_fdw__" muestra, aproximadamente, una .70x sobrecarga en comparación con una tabla local. Aunque, para transacciones de solo lectura, la sobrecarga es mucho mayor, probablemente debido a los efectos del aislamiento de transacción forzado usado (repeteable reado) en FDW como se detalla en FDW F.33.3. Por esta razón, __todas las pruebas se han realizado utilizando el aislamiento REPETEABLE READ__, para que la comparación sea kista (especialmente para cargas de trabajo de solo lectura, si se necesitan más análisis para mantener la coherencia de los resultados).

Además, consideré las estadísticas de TPS, *incluidas las conexiones*, ya que estamos tratando de considerar todas las fases de ejecución. Es importante tener en cuenta que "__postgres_fdw__" recicla las conexiones en la misma sesión para cada asignación de usuario.


### NOTAS

* Si va a tener lecturas intensivas para búsquedas de claves, es posible que prefiera conectarse directamente a los nodos.
* Considere las limitaciones en el aislamiento de transacciones, ya que el uso de FDW tiene un comportamiento diferente al estándar predeterminado de Postgres (lectura confirmada).
* La red puede agregar una latencia considerable. Si es posible, los servidores de bases de datos deben comunicarse en redes rápidas y cerradas, para eivar cualquier otro ruido adicional.
* Tanto los bancos RW como los RO utilizan una conexi una conexión para el pgbench. Hay dos razones para esto:
    * 1) Quería Mantener las pruebas RW / RO comparables en relación y,
    * 2) cuando usa FDW, está conectado a un aislamiento de transacción más alto que el predeterminado, lo que significa que ejecutar más de una conexión RW no poder serializar el acceso debido a la actualización concurrente.

### Sobrecarga de RW por rendimiento de TPS

La sobrecarga estimada entre las operaciones FDW y RW directas es un factor de .70x. Considerando que las pruebas fueron intensivas, es un número muy bueno

### Sobrecarga de RO por rendimiento de TPS

La cabeza de lectura intensiva para cargas de trabajo sólo es significativamente mayor que RW: 6.5x.

Al ejecutar FDW local y externamente, observé que el FDW externo tenía un rendimiento de TPS más inestable, aunque la media no muestra una diferencia significativa. De hecho, a través de todos los puntos de referencia que he realizado, hay un rendimiento ligeramente mayor en la media de TPS cuando se una FDW en una instancia diferente en lugar de usar FDW localmente.

*COLOCAR GRAFICO 1* 

__updatable__ no ayuda en el rendimiento, ya que solo agrega un poco más de sobrecarga debido a la verificación de permisos. "__RO.FDW_ext_ro__" agrega las opciones que se muestra en {Snippet 2} a cada tabla FDW.

Con 1 conexión para RO:

*COLOCAR GRAFICO 2*

Con 5 conexiones para conexiones RO en todas las pruebas:

*COLOCAR GRAFICO 3*

Nota:

Parece que FDW tiene un techo en términos de rendimiento para sesiones simultáneas. Esto probablemente necesite un punto de referencia más intensivo que podría analizarse en una segunda parte de este artículo.

### Inspección de latencia

No es una sorpresa ver una correlación cuando se observa la latencia en milisegundos. Esto se puede ver claramente en la Figura 2

*Colocar grafico FIGURA 2*

### Datos agregados de los puntos de referencia 

TPS por RO y RW:

    > subset(byBenchTPS, Type == "RO")
            Bench       Type        Target      Max         Min          Mean
    1 RO.FDW_ext_ro     RO     FDW_ext_ro     1637.314    1402.255     1507.400
    2 RO.FDW_external   RO     FDW_external   1596.900    1413.704     1519.091
    3 RO.FDW_local      RO     FDW_local      1570.985    1310.034     1476.397
    4 RO.local          RO     local         11670.858   10981.858    11475.249

    >subset(byBenchTPS, Type == "RW")
            Bench       Type        Target     Max          Min          Mean
    1 RW.FDW_external   RW     FDW_external   136.4827    110.6894     124.5781
    2 RW.FDW_local      RW     FDW_local      145.5167    125.2096     133.1675
    3 RW.local          RW     local          240.8039    205.6248     219.2062

### Reproduciendo la Prueba

Poblar pgbench tablas dentro de una escala de 100

    /usr/lib/postgresql/9.6/bin/pgbench -p5434 -i -s100 source

Creando el esquema en las bases de datos desde las que se llaman los FDW (external, external_ro y localfdw):

[Fragmento 1]

    CREATED SERVER source_server FOREIGN DATA WRAPPER postgres_fdw
        OPTIONS (host '127.0.0.1', port '5434', dbname 'source');

    CREATED USER MAPPING FOR postgres SERVER source_server OPTIONS(user 'postgres');

    -- New 9.6 feature!
    IMPORT FOREIGN SCHEMA public LIMIT TO (pgbench_accounts,pgbench_history,pgbench_branches,pgbench_tellers) FROM SERVER source_server INTO public ;

[Fragmento 2] aplicado al "__external_ro__" base de datos:

    ALTER FOREIGN TABLE pgbench_account OPTIONS (updatable 'false');
    ALTER FOREIGN TABLE pgbench_branches OPTIONS (updatable 'false');
    ALTER FOREIGN TABLE pgbench_history OPTIONS (updatable 'false');
    ALTER FOREIGN TABLE pgbench_history OPTIONS (updatable 'false');

Automatizar los puntos de referencia y poner todo en CSV (promedio de latencia, TPS incluidas conexiones y TPS excluidas conexiones):

    PGBENCHBIN="/usr/lib/postgresql/9.6/bin/pgbench"
    PGVACUUM="/usr/lib/postgresql/bin/vacuumdb -p5434 source"
    $PGVACUUM
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5434 -n -T10 source | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRW.local
    $PGVACUUM 
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5434 -n -T10 localfdw | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRW.FDW_local
    $PGVACUUM 
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5435 -n -T10 external | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRW.FDW_external
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5434 -Sn -T10 source | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRO.local 
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5434 -Sn -T10 localfdw | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRO.FDW_local
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5435 -Sn -T10 external | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRO.FDW_external
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5435 -Sn -T10 external_ro | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRW.FDW_ext_ro

    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5434 -Sn -c5 -T10 source | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRO.local_5
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5434 -Sn -c5 -T10 localfdw | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRO.FDW_local_5
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5435 -Sn -c5 -T10 external | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRW.FDW_external_5
    { for i in $(seq 1 10) ; do $PGBENCHBIN -p5435 -Sn -c5 -T10 external_ro | grep -Po '= \K[\d]+\.[\d]+' | paste -sd "," - ;done } > benchRO.FDW_ext_ro_5


¡Espero que hayas disfrutado el artículo!




