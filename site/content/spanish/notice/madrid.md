---
title: "Abrimos oficina en Madrid!"
date: 2021-11-20T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "."
# notice download link
download_link : "#"
# type
type: "notice"
---


### ¡Hola Madrid!

Estamos orgullosos de informar que hemos establecido una nueva oficina en Madrid, España. 

Actualmente funcionamos como Plataform3 S.L.