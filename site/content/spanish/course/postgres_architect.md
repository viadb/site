---
title: "Administración avanzada y arquitectura en Postgres"
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "Nuestros cursos de arquitectura con PostgreSQL, proveen los fundamentos y conocimientos necesarios para extender la funcionalidad de un servidor postgres, bien sea a través de un middleware o escalando la capacidad utilizando múltiples servidores en simultaneo."
# course thumbnail
image: "images/courses/arch1.jpg"
# taxonomy
category: "Base de Datos"
# staff
staff: "Emanuel Calvo"
# duration
duration : "20 Hours"
# weekly
weekly : "20 Hours"
# course fee
fee : "Consulte para precios en ARS"
# apply url
apply_url : "#"
# type
type: "course"
---

## Acerca del curso Avanzado de Administración y Arquitectura en Postgres 

La duración recomendada de esta formación es de 14 a 20 horas, incluidos los laboratorios, con una lista de temas flexible. Nos enfocamos en lo que importa para el trabajo diario y el conocimiento constante en torno al ecosistema de Postgres.

También aceptamos preguntas sobre problemas específicos en su infraestructura y lo ayudaremos a solucionarlo.


### Sistema operativo y Postgres

* Configuración del sistema operativo Linux para Postgres: en este módulo, cubriremos la configuración del sistema operativo Linux que puede afectar el rendimiento y el comportamiento de Postgres, incluidos los parámetros del kernel, los recursos del sistema y la configuración de red.

* Componentes internos de la memoria compartida de Postgres y caché del sistema operativo: en este módulo, profundizaremos en los detalles de cómo Postgres usa la memoria compartida, incluidos los diferentes contextos de memoria, los algoritmos de asignación de memoria y la administración de memoria compartida. Además, cubrirá los mecanismos utilizados para configurar el almacenamiento en caché del sistema operativo.


### Replicación

* Introducción
* Replicación asíncrona
* Envío de registros
* Replicación de transmisión
* Replicación síncrona
* PITR

### Pooling

Pooling con PgBouncer: en este módulo, le presentaremos PgBouncer, un popular pooling de conexiones para Postgres. Te mostraremos cómo instalar y configurar PgBouncer, así como también cómo monitorear y optimizar su rendimiento. 

* ¿Por qué necesito un pooling de conexiones? 
* PgBouncer
* PgPool

### Escalabilidad y disponibilidad

Soluciones de alta disponibilidad con Patroni: En este módulo, nos centraremos en Patroni, una popular solución de código abierto para lograr una alta disponibilidad con Postgres. Le mostraremos cómo configurar y administrar un clúster Patroni, así como también cómo monitorear y solucionar problemas comunes.

* Patroni HA
* Balanceo de carga
* Particionamiento de datos y estrategias de federación

### Almacenamiento en caché:

* memcached 
* pgmemcached

### Postgres containers

* Docker
* Soluciones en Kubernetes disponibles