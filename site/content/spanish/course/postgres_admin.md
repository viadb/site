---
title: "Administración Postgres"
date: 2019-06-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "La formación de Administrador de Postgres proporciona los conocimientos necesarios para evaluar, mantener y operar un sistema de producción de un servidor PostgreSQL como una unidad funcional."
# course thumbnail
image: "images/courses/adm1.jpg"
# taxonomy
category: "Base de Datos"
# staff
staff: "Emanuel Calvo"
# duration
duration : "20 Hours"
# weekly
weekly : "20 Hours"
# course fee
fee : "Consulte para precios en ARS"
# apply url
apply_url : "#"
# type
type: "course"
---


## Sobre el curso de Administración Postgres

> No cotizamos nuestras capacitaciones por persona, sino que puede cambiar de manera flexible la cantidad de horas de capacitación, y la cobertura del contenido se ajustará al ritmo de su equipo.

La formación cubre los siguientes tópicos:

* Instalación/Configuración del Servidor
* Elección de hardware / Evaluación
* Prueba de rendimiento (benchmarking)
* Diseño de base de datos, objetos soportados
* Integridad referencial
* Mantenimiento
* Copias de seguridad
* Vista de vuelo de las métricas de monitoreo más comunes


La duración recomendada de esta formación es de 14 a 20 horas, incluidos los laboratorios, con una lista de temas flexible. Nos enfocamos en lo que importa para el trabajo diario y el conocimiento constante en torno al ecosistema de Postgres.

También aceptamos preguntas sobre problemas específicos en su infraestructura y lo ayudaremos a solucionarlo.

Los temarios no son fijos y puede solicitar profundizar en temas específicos más que en otros; piense en nuestros entrenamientos más como un AMA estructurado (_Ask Me Anything_).



## Temario

### Instalación/configuración inicial


* Instalación y Configuración
     * Versiones Disponibles
     * Opcional: compilación fuente
     * Sistema de embalaje
     * Actualización
* Hardware:
     * CPU y memoria
     * Hardware de almacenamiento, FS, partición. RAID, LVM, etc
     * Planificación de recursos relacionados con la base de datos en la nube y servicios disponibles.

### Aspectos de diseño

* El modelo relacional. Fortalezas y debilidades.
* Tablas, tipos de datos y otros objetos.
* Índice y tipos de métodos de acceso.
* Integridad Referencial (Claves Foráneas)
* Vistas: ocultar complejidades de implementación

### Sistemas de respaldo

* pgrespaldo
* También se pueden incluir otras soluciones de respaldo como barman.


### Monitorización

* Métricas más comunes para Postgres.
* Análisis del servidor:
     * Herramientas del sistema operativo para el monitoreo de recursos (sysstat, *top, etc.)
     * Herramientas PostgreSQL
     * Sistema de tablas y Vistas
     * Herramientas externas (Prometheus, Elastic Stack, etc.)
     * Análisis de registros. pgbadger
* Consulta de análisis de muestreo: pg_stat_statements
* Análisis de registro
* Copias de seguridad

### Análisis de rendimiento

* Análisis de nivel de sistema operativo
     * Uso de CPU
     * Memoria
     * Uso de caché e invalidación por el _dirty cleaner_
* Uso de Explain para conocer el plan de ejecución
* Consultas lentas 
* Ajustando la configuración de Postgres
     * Memoria del servidor
     * Memoria interna
     * Conexiones
     * Configuración WAL
     * Puesto de control y ayudantes
* Todo sobre la indexación:
     * Uso efectivo de índices
     * Índices parciales
     * Índices insuficientes
     * ¿Cuándo _demasiados índices_ es un problema?
     * Hinchazón del índice
     * Agrupación
     * Evaluación de la eficacia de los índices
     * Las tablas del catálogo `pg_stat_*`
* Estrategias de Vacuum
* Checkpoints
* Bulk loading

### Seguridad

* El archivo pg_hba.conf
* Privilegios SQL
* La Cláusula de Invocador/Definidor de Seguridad
* Conexiones vía SSL

