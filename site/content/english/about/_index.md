---
title: "About Us"
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# about image
image: "images/logos-new/new-logoviadb-1.png"
# meta description
description : "Description"

---


### About us

ViaDB is a consulting firm that aims to provide consulting, management and development of programmatic Database Infrastructure (IaC) services on top of the major available Open Source Databases.

ViaDB is located in Argentina (Buenos Aires), and Spain (Madrid).

We're engaged to bring international level quality to our services and technologies based the experience around large and global infraestructures. 

> We are local partners in Argentina of __OnGres Inc.__, a company with a growing global reputation.

Established in the market for more than 10 years, adding members to the team and starting a new stage in the company, establishing new strategic partners. All of our professionals have worked in internationally renowned companies, manipulated and developed highly complex infrastructures.

__We are proud of what we have accomplished so far and look forward to seeing it in our client portfolio!__
