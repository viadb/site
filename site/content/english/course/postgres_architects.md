---
title: "Postgres Advanced Administration and Arquitecture"
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "The PostgreSQL Architect courses provide the fundamentals and knowledge necessary to extend the functionality of a postgres server, either through middleware or by scalinbg the capacity using multiple servers simultaneously."
# course thumbnail
image: "images/courses/arch1.jpg"
# taxonomy
category: "Databases"
# staff
staff: "Emanuel Calvo"
# duration
duration : "20 Hours"
# weekly
weekly : "20 hours"
# course fee
fee : "From: $USD 2500"
# apply url
apply_url : "#"
# type
type: "course"
---

 
## About the Postgres Advanced Administration and Arquitecture

The recommended duration for this training is between 14 and 20 hours, including laboratories, with a flexible topic list. We focus on what it matters for the day to day work and consistent knowledge around the Postgres ecosystem.

We also accept questions about specific problems in your infrastructure, and we'll assist you through its troubleshooting.


### OS and Postgres

* Linux OS settings for Postgres: In this module, we will cover the Linux OS settings that can affect the performance and behavior of Postgres, including kernel parameters, system resources, and networking configuration.

* Postgres shared memory internals and OS cache: In this module, we will dive into the details of how Postgres uses shared memory, including the different memory contexts, memory allocation algorithms, and shared memory management. Also, it will cover the mechanisms used for configuring OS caching.


### Replication

* Introduction
* Asynchronous Replication
* Log Shipping
* Streaming Replication
* Synchronous Replication
* PITR

### Pooling

Pooling with PgBouncer: In this module, we will introduce you to PgBouncer, a popular connection pooler for Postgres. We will show you how to install and configure PgBouncer, as well as how to monitor and optimize its performance.

* Why do I need a Connection Pooler?
* PgBouncer
* PgPool

### Scalability and availability

High availability solutions with Patroni: In this module, we will focus on Patroni, a popular open-source solution for achieving high availability with Postgres. We will show you how to set up and manage a Patroni cluster, as well as how to monitor and troubleshoot common issues.

* Patroni HA
* Load Balancing 
* Data Partitioning and Federating Strategies 

### Caching:

* memcached 
* pgmemcached

### Postgres containers

* Docker
* Kubernetes available solutions