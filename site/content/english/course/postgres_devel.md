---
title: "SQL / PLSQL Developers "
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "The PostgreSQL Developer courses are aimed at programmers and system developers in general."
# course thumbnail
image: "images/courses/dev.jpg"
# taxonomy
category: "Programming"
# staff
staff: "Emanuel Calvo"
# duration
duration : " Month"
# weekly
weekly : " hours"
# course fee
# fee : "From: $"
# apply url
apply_url : "#"
# type
type: "course"
draft: true
---


### About Course

Graduates of the "PostgreSQL Developer" Courses will have a strong knowledge of 

    - SQL Language
    - Stored Procedures
    - PL / SQL
    - Triggers
    - Views
    - Transactions

Which puts them in an excellent position to develop complex and powerful functionality in the PostgreSQL engine.


### Temary

First Module: SQL Language - Part I

* Introduction
* Common data types
* Basic Statements

     * INSERT
     * DELETE
     * UPDATE
     * SELECT

* Additional Clauses

     * WHERE
     * IN
     * ANY / ALL
     * NOT IN
     * LIKE
     * ~
     * ORDER BY
     * LIMIT
     * OFFSET

First Module: SQL Language - Part II    

* JOINs

    * INNER JOIN
    * LEFT JOIN
    * RIGHT OUTER JOIN
    * CROSS JOIN

* SUB-SELECTs / Nested Queries 
* Aggregates / GROUP BY Clause

Second Module : Views / Stored Procedures / Functions

* Introduction
* Views 
* Stored Procedures / Functions

    * Parameters / Arguments
    * IN / OUT / IMPUT parameters
    * Default Values
    * Features Overloads
    * Security Options: SECURITY DEFINER / INVOKER
    * Durability Options: VOLATILE / STABLE / INMUTABLE

Third Module: PL / SQL

* PL / SQL

    * Introduction
    * "Procedural" Structures
    * Conditionals: IF / ELSE / ELSEIF
    * Cyclic: FOR / WHILE / EXIT / CONTINUE
    * Catching Errors: BEGIN / EXCEPTION 
    * SELECT INTO
    * PERFORM 
    * RETURN NEXT
    * RETURN QUERY

Fourth Module: Advanced Features

* Transactions
* The ACID Specification
* Triggers

    * Introduction
    * Types of Triggers: INSERT / DELETE / UPDATE
    * Trigger declaration 
    * OLD and NEW
    * Arguments in trigger functions
    * BEGIN ... END / ROLLBACK

Fifth Module: Advanced SQL

* INSERT .. RETURNING
* DELETE .. RETURNING
* Temporal Tables / WITH / CTE
* Construction
* Window Functions
* Advanced Filters with Regular Expressions