---
title: "Emanuel Calvo"
# page title background image
bg_image: "images/staff/emanuel.jpg"

# meta description
description : "ViaDB Founder, and working in the Database Reliability Engineering field and Architecture at OnGres."
email: "emanuel@viadb.ar"
# portrait
image: "images/staff/emanuel.jpg"
contact:
  # contact item loop
  - name : "emanuel@viadb.ar"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "mailto:emanuel@viadb.ar"

  # contact item loop
  # - name : "+12 034 5876"
  #   icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
  #   link : "tel:+120345876"

  # contact item loop
  # - name : "Clark Malik"
  #   icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
  #   link : "#"

  # contact item loop
  - name : "Website"
    icon : "ti-world" # icon pack : https://themify.me/themify-icons
    link : "http://tr3s.ma"

  - name : "3manuek"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/3manuek"

  - icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "https://github.com/3manuek"
  
  - icon : "ti-linkedin"
    link : "https://www.linkedin.com/in/ecbcbcb/"
---

Founder and CEO of ViaDB and Database Reliability Engineer and Architect at OnGres.

He also leads the IT Team at Fundación Apolo.
