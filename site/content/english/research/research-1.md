---
title: "Stay tuned for our next White Papers!"
date: 2019-07-06T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : ""
# Research image
image: "images/research/bluecircuit.jpg"
# type
type: "research"
# draft: true
---

## About Our Researches

Our consultants have conducted researches across many companies and technologies. We'll continue working to publish original content in the field, from Infrastucture to Database Internals.
