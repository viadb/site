---
title: "Emanuel Calvo"
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description: "Founder"
# staff portrait
image: "images/staff/emanuel.jpg"
# course
course: "Database Architect"
# biography
bio: "Founder of ViaDB and working in the Database Reliability Engineering field and Architecture for OnGres."
# interest
interest: ["Computer Networking","DataBase Infraestructure","Database Architect"]
# contact info
contact:
  # contact item loop
  - name : "emanuel@viadb.ar"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "mailto:emanuel@viadb.ar"

  # contact item loop
  # - name : "+12 034 5876"
  #   icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
  #   link : "tel:+120345876"

  # contact item loop
  # - name : "Clark Malik"
  #   icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
  #   link : "#"

  # contact item loop
  - name : "Website"
    icon : "ti-world" # icon pack : https://themify.me/themify-icons
    link : "http://tr3s.ma"

  - name : "3manuek"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/3manuek"

  - icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "https://github.com/3manuek"
  
  - icon : "ti-linkedin"
    link : "https://www.linkedin.com/in/ecbcbcb/"

type: "staff"
tipo: "founder"
---

### About Me

Emanuel Calvo is the Plataform3 SL founder, holding ViaDB and dmob.dev projects. He is a Database Reliability Engineer with experience in a large variety of high-end Production environments, through CI/CD systems (AWS, GCP, Azure, Kubernetes, Terraform, Ansible, etc).

He has DCS cluster technologies experience, for distributed consensus computing and reliability strategies. Not exclusively to databases, he also performs OS low-level tweaks for highly customized environments.

He worked in positions related to different engines such as Postgres, MySQL, Elastic, MongoDB, and MariaDB at consulting firms such as OnGres, Percona, Pythian, 2ndQuadrant, and Blackbird (now Pythian, ex-PalominoDB). He managed operations in Microsoft, Amazon, ARM, Zendesk, Hyperwallet, Atlassian, BBM, Gitlab, Mozilla, Adobe Echosign, and Fitbit, among others.

As Director of IT at Fundación Apolo, he is in charge of resource managing, maintenance, and site development for several projects.

He co-founded CanalDBA, a Spanish spoken Latam community about Database Nerds.