---
title: "Pablo Agustín Lencinas"
draft: false
# page title background image
bg_image: "images/backgrounds/data.jpg"
# meta description
description : "Developer"
# staff portrait
image: "images/staff/icon.jpg"
# course
# course: "Database Architect"
# biography
bio: "Developer Jr. de ViaDB"
# interest
interest: ["Computer Networking","Database Infraestructure","Database Architect"]
# contact info
contact:
  # contact item loop
  - name : "emanuel@viadb.ar"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "mailto:emanuel@viadb.ar"

  # contact item loop
  # - name : "+12 034 5876"
  #   icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
  #   link : "tel:+120345876"

  # contact item loop
  # - name : "Clark Malik"
  #   icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
  #   link : "#"

  # contact item loop
  - name : "Website"
    icon : "ti-world" # icon pack : https://themify.me/themify-icons
    link : "http://3manuek.com"

  - name : "3manuek"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/3manuek"

  - icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "https://github.com/3manuek"
  
  - icon : "ti-linkedin"
    link : "https://www.linkedin.com/in/ecbcbcb/"

type: "staff"
---

### About Me

